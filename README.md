# Numeral.lua

Convert numbers to number words ( or vice versa ). __*เฉพาะภาษาไทย__

## Usage:
### numeral.num2text()
**[in]:**
```lua
local num2text = require("numeral").num2text
print(num2text(-2345, {money=true}))
print(num2text(-2011.1234))
local inspect = require("inspect")
print(inspect(num2text(123456721.12645, {mode="t", money=true})))
```
**[out]:**
```
ลบสองพันสามร้อยสี่สิบห้าบาทถ้วน
ลบสองพันสิบเอ็ดจุดหนึ่งสองสามสี่
{
  fract = "สิบสามสตางค์",
  int = "หนึ่งร้อยยี่สิบสามล้านสี่แสนห้าหมื่นหกพันเจ็ดร้อยยี่สิบเอ็ดบาท",
  sep = "",
  sign = "",
  words = "หนึ่งร้อยยี่สิบสามล้านสี่แสนห้าหมื่นหกพันเจ็ดร้อยยี่สิบเอ็ดบาทสิบสามสตางค์"
}
```
### numeral.text2num()
**[in]:**
```lua
local text2num = require("numeral").text2num
print(text2num("ห้าแสนสองร้อยสิบ"))
print(text2num("ลบหนึ่งสองสามสี่"))
print(text2num("สิบเอ็ดล้านห้าแสนสองร้อยสิบล้านห้าหมื่นยี่สิบจุดหนึ่งสองสามสี่"))
print(text2num("จุดหนึ่งสองสามสี่", {money=true}))
```
**[out]:**
```
500210
-1234
11500210050020.1234
0 บาท 1234 สตางค์
```
