--- Numeral.lua แปลงค่าระหว่างตัวเลขกับคำอ่าน
--
-- @module numeral
-- @release 0.0.1
local numeral = {}
local th_digit = {"", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน",}
local th_numtxt = {
  ["0"]="ศูนย์",
  ["1"]="หนึ่ง",
  ["2"]="สอง",
  ["3"]="สาม",
  ["4"]="สี่",
  ["5"]="ห้า",
  ["6"]="หก",
  ["7"]="เจ็ด",
  ["8"]="แปด",
  ["9"]="เก้า",
  ["ศูนย์"]="0",
  ["หนึ่ง"]="1",
  ["สอง"]="2",
  ["สาม"]="3",
  ["สี่"]="4",
  ["ห้า"]="5",
  ["หก"]="6",
  ["เจ็ด"]="7",
  ["แปด"]="8",
  ["เก้า"]="9",
}
local th_txt = {
  "ศูนย์", "หนึ่ง", "เอ็ด", "สอง", "ยี่", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า",
}
local toint = math.tointeger


-- หาคำอ่านจำนวนเต็ม หรือทศนิยมแบบค่าเงิน
local function th_int2text(num)
  if tonumber(num) == 0 then return "ศูนย์" end
  if tonumber(num) == 1 then return "หนึ่ง" end
  local words = ""
  local digit = 1
  -- วนลูปจากหลักหน่วย (หลังไปหน้า)
  for i = #num, 1, -1 do
    local str = num:sub(i,i)
    if str == "0" then
      str = ""
    -- ถ้าทุกหลักหน่วยหรือหลักล้านและยังมีตัวเลขข้างหน้าให้ใช้ "เอ็ด" แทน "หนึ่ง"
    elseif digit % 6 == 1 and i > 1 and str == "1" then
      str = "เอ็ด"..th_digit[digit]
    -- ถ้าไม่ใช่หลักสิบหรือตัวเลขมากกว่า 2 ให้แปลงตามค่าใน th_numtxt
    elseif digit % 6 ~= 2 or str > "2" then
      str = th_numtxt[str]..th_digit[digit]
    else
      if str == "1" then
        str = "สิบ"
      elseif str == "2" then
        str = "ยี่สิบ"
      end
    end
    words = str..words
    digit = digit < 7 and digit + 1 or 2   
  end
  return words
end

-- หาคำอ่านทศนิยมแบบตัวเลขปกติ (ไม่ใช่ค่าเงิน)
local function th_fraction2text(num)
  return num:gsub("%d",th_numtxt)
end

-- กำหนด format
local function set_format(fmt)
  if fmt and type(fmt) == "table" then
    local money = fmt.money
    local main_unit = fmt.main or "บาท"
    local sub_unit = fmt.sub or "สตางค์"
    local round = money and 2 or toint(fmt.round)
    local mode = fmt.mode or "s"
    return money, main_unit, sub_unit, round, mode
  end
end

-- เอาข้อความส่วนเกินที่ไม่ใช่ตัวเลขออก
local function remove_none_numtext(num)
    local tmp = num
    for i = 1, #th_txt do
      tmp = tmp:gsub(th_txt[i], " "..th_txt[i].." ")
    end
    for i = 2, #th_digit do
      tmp = tmp:gsub(th_digit[i], " "..th_digit[i].." ")
    end
    tmp = tmp:gsub("%s([^%s]+)%s", " ")
    for w in tmp:gmatch("[^%s]+") do
      num = num:gsub(w, "")
    end
    return num
end

-- แยกตัวเลขทีละหลัก
local function split_int(words, key, sum)
  local n1, n2 = words:match("(.-)"..th_digit[key].."(.*)")
  if n1 then
    -- กรณีหลักสิบ
    if key == 2 then
      if n1 == "" then return n2, sum + 10 end
      if n1 == "ยี่" then return n2, sum + 20 end
    end
    -- แปลงเป็นตัวเลข
    n1 = toint(n1:gsub(".+", th_numtxt))
    if n1 then
      return n2, sum + (n1 * (10 ^ (key - 1)))
    end
  end
  return words, sum
end

-- แปลงคำอ่านเป็นตัวเลขแบบใช้หลัก
local function th_text2num(words, numtext)
  -- แยกส่วนหน้าหลัง "ล้าน" ออกมา
  local n1, n2 = words:match("(.-)ล้าน(.*)")
  local n3, n4
  local sum = 0
  if not n1 then
    n3 = words
  else
    n3, n4 = n1, n2
  end
  -- หาหลักแสนถึงหลักสิบ
  for i = 6, 2, -1 do
    n3, sum = split_int(n3, i, sum)
  end
  -- หาตผลรวมตัวเลขหลักหน่วยกับก่อนหน้า
  if n3 and n3 ~= "" then
    if n3 == "เอ็ด" then
      sum = sum + 1
    else
      n3 = toint(n3:gsub(".+", th_numtxt))
      if n3 then
        sum = sum + n3
      else
        error("ไม่สามารถแปลง '"..words.."' เป็นตัวเลขได้")
      end
    end
  end
  -- แปลงกลับเป็นตัวหนังสือ
  sum = tostring(toint(sum))
  -- กรณีที่มีตัวเลขที่แปลงแล้วก่อนหน้า
  if numtext then
    -- ถ้าตัวเลขชุดปัจจุบันน้อยกว่าแสนให้เติม 0 ข้างหน้า
    for i = #sum, 5 do
      sum = "0"..sum
    end
    -- รวมตัวเลขก่อนหน้ากับเลขปัจจุบัน
    sum = numtext..sum
  end
  -- ถ้ายังมีข้อความที่ยังไม่ได้แปลงให้ทำ recursion
  if n4 then
    return th_text2num(n4, sum)
  end
  return sum
end

-- แปละคำอ่านเป็นตัวเลขแบบไม่ใช้หลัก
local function th_simple_text2int(words)
  words = remove_none_numtext(words)
  local tmp = words
  for i = 1, #th_txt do
    tmp = tmp:gsub(th_txt[i], th_numtxt)
  end
  if tonumber(tmp) then return tmp end
  return th_text2num(words)
end

--- Convert numbers to words
--
-- @num numbers
-- @fmt format {[money=[true|false]][, main=["หน่วยเงินหลัก"]][, sub=["หน่วยเงินรอง"]][, round=[จำนวนทศนิยม]][, mode=[return type "t"|"s" (table หรือ string)]],}
-- @return คำอ่านเป็น string หรือ table เก็บคำอ่าน {sign=["ลบ"|nil], int=["จำนวนเต็ม"|"เงินหน่วยหลัก"], sep=["จุด"|""], fract=["ทศนิยม"|"เงินหน่วยย่อย"], words=["คำอ่าน"]}
function numeral.num2text(num, fmt)
  if not tonumber(num) then return nil end
  local num_string = tostring(num)
  -- กำหนด format
  local money, main_unit, sub_unit, round, mode = set_format(fmt)
  -- กำหนดจำนวนทศนิยม
  if round then num_string = ("%."..round.."f"):format(num_string) end
  -- แยกคำอ่านเครื่องหมายลบ
  local sign = num_string:sub(1,1) == "-" and "ลบ" or ""
  -- แยกจำนวนเต็มกับทศนิยม
  local int,fraction = num_string:match("(%d+)%.?(%d*)")
  local sep = ""
  -- หาคำอ่านจำนวนเต็ม
  int = th_int2text(int)
  -- หาคำอ่านทศนิยม
  -- กรณีเป็นค่าเงิน
  if money then
    int = int..main_unit
    fraction = th_int2text(fraction)
    if fraction == "ศูนย์" or fraction == "" then
      fraction = ""
      int = int.."ถ้วน"
    else
      fraction = fraction..sub_unit
    end
  -- กรณีไม่ใช่ค่าเงิน
  else
    fraction = th_fraction2text(fraction)
    if fraction ~= "" then
      sep = "จุด"
    end
  end
  local words = sign..int..sep..fraction
  if mode == "t" then
    local result = {
      sign = sign,
      int = int,
      sep = sep,
      fract = fraction,
      words = words,
    }
    return result
  end
  return words
end

--- Thai words to numbers
--
-- @words string
-- @fmt format {[money=[true|false]][, main=["หน่วยเงินหลัก"]][, sub=["หน่วยเงินรอง"]])
-- @return ตัวเลขเป็น string 
function numeral.text2num(words, fmt)
  -- แยกเครื่องหมาย
  local sign = words:match("^ลบ")
  sign = sign and "-" or ""
  local money, main_unit, sub_unit = set_format(fmt)
  local int, fract
  -- กรณีเป็นค่าเงินแยกหน่วย
  if money then
    int, fract = words:match("^(.*)"..main_unit.."(.*)"..sub_unit.."$")
    if not int then
      int = words:match("^(.*)"..main_unit..".*$")
      fract = words:match("^(.*)"..sub_unit.."$")
    end
  end
  if not int and not fract then
    int, fract = words:match("^(.*)จุด(.*)$")
    if not int then int = words end
  end
  if int then
    int = th_simple_text2int(int)
  end
  if fract then
    fract = th_simple_text2int(fract)
  end
  local num
  if money then
    int = int and int.." "..main_unit.." " or ""
    fract = fract and fract.." "..sub_unit or ""
    return sign..int..fract
  end
  return sign..(int ~= "" and int or "0")..(fract and "."..fract or "")
end
 
return numeral
